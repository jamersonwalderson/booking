# -*- encoding: utf-8 -*-
import pytest

from dateutil.relativedelta import relativedelta
from django.utils import timezone

from booking.models import Booking, Permission
from .factories import BookingFactory, CategoryFactory


@pytest.mark.django_db
def test_public_calendar():
    user = Permission.objects.get(slug=Permission.USER)
    staff = Permission.objects.get(slug=Permission.STAFF)
    # default permission is 'public'
    BookingFactory(title="a")
    BookingFactory(title="b", permission=user)
    BookingFactory(title="c", permission=staff)
    BookingFactory(title="d")
    events = Booking.objects._public_calendar().order_by("title")
    assert ["a", "d"] == [e.title for e in events]


@pytest.mark.django_db
def test_public_calendar_date():
    """Select published events within the next two months."""
    today = timezone.now().date()
    b4 = today + relativedelta(days=-1)
    one = today + relativedelta(days=7)
    two = today + relativedelta(days=14)
    year = today + relativedelta(years=1)
    start = timezone.now().time()
    BookingFactory(title="a", start_date=one, start_time=start)
    BookingFactory(title="b", start_date=two, start_time=start)
    # do NOT include this one because it is more than two months in future.
    BookingFactory(title="c", start_date=year, start_time=start)
    # do NOT include this one because it for yesterday
    BookingFactory(title="d", start_date=b4, start_time=start)
    events = Booking.objects._public_calendar()
    assert ["a", "b"] == [e.title for e in events]


@pytest.mark.django_db
def test_public_delete():
    today = timezone.now().date()
    one = today + relativedelta(days=7)
    start = timezone.now().time()
    BookingFactory(title="a", start_date=one, start_time=start)
    # do NOT include this one because it is deleted
    BookingFactory(
        title="b",
        start_date=one,
        start_time=start,
        deleted=True,
    )
    events = Booking.objects._public()
    assert ["a"] == [e.title for e in events]


@pytest.mark.django_db
def test_public_promoted():
    """Promoted events are between two and eight months."""
    today = timezone.now().date()
    one = today + relativedelta(days=7)
    six = today + relativedelta(months=6)
    year = today + relativedelta(years=1)
    promote = CategoryFactory(promote=True)
    routine = CategoryFactory(promote=False, routine=True)
    start = timezone.now().time()
    # do NOT include this one because it is less than 2 months
    BookingFactory(title="a", start_date=one, start_time=start)
    BookingFactory(
        title="b",
        start_date=six,
        start_time=start,
        category=promote,
    )
    # do NOT include this one because it is a routine event (not promoted)
    BookingFactory(
        title="c",
        start_date=six,
        start_time=start,
        category=routine,
    )
    # do NOT include this one because it is older than 8 months
    BookingFactory(title="d", start_date=year, start_time=start)
    # do NOT include this one because it is deleted
    BookingFactory(
        title="e",
        start_date=six,
        start_time=start,
        deleted=True,
    )
    # do NOT include this one because it is not published
    BookingFactory(title="e", start_date=six, start_time=start)
    events = Booking.objects.public_promoted()
    assert ["b"] == [e.title for e in events]


# def test_public_status():
#    today = timezone.now().date()
#    one = today + relativedelta(days=7)
#    public = PermissionFactory(slug=Permission.PUBLIC)
#    #publish = StatusFactory(publish=True)
#    #pending = StatusFactory(publish=False)
#    start = timezone.now().time()
#    BookingFactory(
#        title='a', start_date=one, start_time=start, #status=pending,
#        permission=public,
#    )
#    BookingFactory(
#        title='b', start_date=one, start_time=start, #status=publish,
#        permission=public,
#    )
#    events = Booking.objects._public()
#    assert ['b'] == [e.title for e in events]
#    )
